﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> clientes = new List<Cliente>();
        private implements.DaoImplementsCliente daocliente;

        public ClienteModel()
        {
            daocliente = new implements.DaoImplementsCliente();
        }
        public List<Cliente> GetClientes()
        {
            return daocliente.findAll();
        }

        public void Populate()
        {
            Cliente[] clts =
            {
                new Cliente(1,"111-222222-3333A","Armando ","Paredes","2233-4455", "armandoparedes@gmail.com","Junto a su vecino"),
                new Cliente(2,"113-222255-3333A","Zacarias ","Piedras del Rio","2244-4895", "piedrasdelrio@gmail.com","Frente a su vecino"),
                new Cliente(3,"222-227582-3333A","Soila ","Vaca","2533-4655", "vacasoi@gmail.com","Detras de su vecino"),
                new Cliente(4,"111-986512-1245","Pepe ","Grillo","2356-4455", "grillopepe@gmail.com","Junto al otro vecino")
            };

            foreach (Cliente c in clientes)
            {
                daocliente.save(c);
            }

            clientes = clts.ToList();

        }
    }
}
