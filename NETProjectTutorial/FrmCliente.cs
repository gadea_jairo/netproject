﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {

        private DataTable tblClientes ;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataTable TblProductos { set { tblClientes = value; } }
        public DataSet DsProductos { set { dsClientes = value; } }
        public DataRow DrProducto
        {
            set
            {
                drCliente = value;
                txtid.Text = drCliente["Id"].ToString();
                txtcedula.Text = drCliente["Cédula"].ToString();
                txtnombres.Text = drCliente["Nombres"].ToString();
                txtapellidos.Text = drCliente["Apellidos"].ToString();
                txttelefono.Text = drCliente["Telefono"].ToString();
                txtcorreo.Text = drCliente["Correo"].ToString();
                txtdireccion.Text = drCliente["Direccion"].ToString();
            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            string cedula, nombres, apellidos, telefono, correo, direccion;

            
            cedula = txtcedula.Text;
            nombres = txtnombres.Text;
            apellidos = txtapellidos.Text;
            telefono = txttelefono.Text;
            correo = txtcorreo.Text;
            direccion = txtdireccion.Text;
            

            if (drCliente != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drCliente);
                drNew["Id"] = drCliente["Id"];
                drNew["Cedula"] = cedula;
                drNew["Nombres"] = nombres;
                drNew["Apellidos"] = apellidos;
                drNew["Telefono"] = telefono;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;


                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);

            }
            else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1, cedula, nombres, apellidos, telefono, correo, direccion);
            }

            Dispose();
        }
    }
}
